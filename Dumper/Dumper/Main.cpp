#include "HMain.h"
#include <sstream> 

std::string OffsetsResult(void)
{
	if (!pProcess->Attach("csgo.exe"))
		return "-1";

	if (!pNetVarManager->Load())
		return "-2";
		
	std::stringstream value;
	value << pNetVarManager->Dump(false).str();
	value << pOffsetManager->Dump(false).str();

	delete pOffsetManager;

	pNetVarManager->Release();
	delete pNetVarManager;

	pProcess->Detach();
	delete pProcess;

	return value.str();
}

int main( void )
{
    if( !pProcess->Attach( "csgo.exe" ) )
        return -1;

    if( !pNetVarManager->Load( ) )
        return -2;

	pNetVarManager->Dump(true);
	pOffsetManager->Dump(true);

    delete pOffsetManager;

    pNetVarManager->Release( );
    delete pNetVarManager;

    pProcess->Detach( );
    delete pProcess;
}