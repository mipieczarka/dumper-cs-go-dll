#include "..\..\Dumper\Dumper\HMain.h"
#include "..\..\Dumper\Dumper\Main.cpp"
#include "..\..\Dumper\Dumper\Include\HWin.h"
#include "..\..\Dumper\Dumper\NetVarManager\HNetVarManager.h"
#include "..\..\Dumper\Dumper\NetVarManager\NetVarManager.cpp"
#include "..\..\Dumper\Dumper\OffsetManager\HOffsetManager.h"
#include "..\..\Dumper\Dumper\OffsetManager\OffsetManager.cpp"
#include "..\..\Dumper\Dumper\Remote\HRemote.h"
#include "..\..\Dumper\Dumper\Remote\Remote.cpp"
#include "..\..\Dumper\Dumper\Utilis\HUtilis.h"

extern "C"
{
	__declspec(dllexport) void Result(char *value)
	{
		std::string str = OffsetsResult();
		std::strcpy(value, str.c_str());
	}
}


