﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DumperApp
{
    public partial class MainForm : Form
    {
        private bool loadOffsetsRichTextBox = true;

        public MainForm()
        {
            InitializeComponent();
            InitOffsets();
            InitClassOffsets();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            StringBuilder offsets = new StringBuilder();
            if (loadOffsetsRichTextBox)
            {                
                int i = 0;
                foreach (var value in Program.Offsets)
                {
                    offsets.AppendLine(string.Format("{0} = {1} -> {2} [{3}]", i, value.Key, value.Value.Item1, value.Value.Item2));
                    i++;
                }
            }
            m_tbOffsets.Text = offsets.ToString();
        }

        private void InitOffsets()
        {
            OffsetsCSGO.Misc.EntityList = Program.Offsets["ENTITYLISTMDWENTITYLIST"].Item2;
            OffsetsCSGO.Misc.LocalPlayer = Program.Offsets["LOCALPLAYERMDWLOCALPLAYER"].Item2;
            OffsetsCSGO.Misc.Jump = Program.Offsets["EXTRAMDWFORCEJUMP"].Item2;
            OffsetsCSGO.Misc.GlowManager = Program.Offsets["EXTRAMDWGLOWOBJECT"].Item2;
            OffsetsCSGO.Misc.SignOnState = Program.Offsets["CLIENTSTATEMDWINGAME"].Item2;
            OffsetsCSGO.Misc.WeaponTable = Program.Offsets["WEAPONTABLEMDWWEAPONTABLE"].Item2;
            OffsetsCSGO.Misc.ViewMatrix = Program.Offsets["ENGINERENDERMDWVIEWMATRIX"].Item2;

            OffsetsCSGO.ClientState.Base = Program.Offsets["CLIENTSTATEMDWCLIENTSTATE"].Item2;
            OffsetsCSGO.ClientState.m_dwInGame = Program.Offsets["CLIENTSTATEMDWINGAME"].Item2;
            OffsetsCSGO.ClientState.m_dwViewAngles = Program.Offsets["CLIENTSTATEMDWVIEWANGLES"].Item2;
            OffsetsCSGO.ClientState.m_dwMapname = Program.Offsets["CLIENTSTATEMDWMAPNAME"].Item2;
            OffsetsCSGO.ClientState.m_dwMapDirectory = Program.Offsets["CLIENTSTATEMDWMAPDIRECTORY"].Item2;

            OffsetsCSGO.GameResources.Base = 0x04A38E2C;
            OffsetsCSGO.GameResources.Names = 0x9D0;
            OffsetsCSGO.GameResources.Kills = 0xBD8;
            OffsetsCSGO.GameResources.Assists = 0xCDC;
            OffsetsCSGO.GameResources.Deaths = 0xDE0;
            OffsetsCSGO.GameResources.Armor = 0x182C;
            OffsetsCSGO.GameResources.Score = 0x192C;
            OffsetsCSGO.GameResources.Clantag = 0x4110;

            OffsetsCSGO.NetVars.C_BaseEntity.m_iHealth = Program.Offsets["DTBASEPLAYERMIHEALTH"].Item2;
            OffsetsCSGO.NetVars.C_BaseEntity.m_iID = Program.Offsets["BASEENTITYMDWINDEX"].Item2;
            OffsetsCSGO.NetVars.C_BaseEntity.m_iTeamNum = Program.Offsets["DTBASEENTITYMITEAMNUM"].Item2;
            OffsetsCSGO.NetVars.C_BaseEntity.m_vecOrigin = Program.Offsets["DTBASEENTITYMVECORIGIN"].Item2;
            OffsetsCSGO.NetVars.C_BaseEntity.m_angRotation = Program.Offsets["DTBASEENTITY_MANGROTATION"].Item2;
            OffsetsCSGO.NetVars.C_BaseEntity.m_bSpotted = Program.Offsets["DTBASEENTITYMBSPOTTED"].Item2;
            OffsetsCSGO.NetVars.C_BaseEntity.m_bSpottedByMask = Program.Offsets["DTBASEENTITYMBSPOTTEDBYMASK"].Item2;
            OffsetsCSGO.NetVars.C_BaseEntity.m_hOwnerEntity = Program.Offsets["DTBASEENTITYMHOWNERENTITY"].Item2;
            OffsetsCSGO.NetVars.C_BaseEntity.m_bDormant = Program.Offsets["BASEENTITYMBDORMANT"].Item2;

            OffsetsCSGO.NetVars.C_CSPlayer.m_lifeState = Program.Offsets["DTBASEPLAYERMLIFESTATE"].Item2;
            OffsetsCSGO.NetVars.C_CSPlayer.m_iArmor = Program.Offsets["DTCSPLAYERMARMORVALUE"].Item2;
            OffsetsCSGO.NetVars.C_CSPlayer.m_hBoneMatrix = Program.Offsets["BASEENTITYMDWBONEMATRIX"].Item2;
            OffsetsCSGO.NetVars.C_CSPlayer.m_hActiveWeapon = Program.Offsets["DTBASEPLAYERMHACTIVEWEAPON"].Item2;
            OffsetsCSGO.NetVars.C_CSPlayer.m_iFlags = Program.Offsets["DTBASEPLAYERMFFLAGS"].Item2;
            OffsetsCSGO.NetVars.C_CSPlayer.m_hObserverTarget = Program.Offsets["DTBASEPLAYER_MHOBSERVERTARGET"].Item2;
            OffsetsCSGO.NetVars.C_CSPlayer.m_iObserverMode = Program.Offsets["DTBASEPLAYER_MIOBSERVERMODE"].Item2;
            OffsetsCSGO.NetVars.C_CSPlayer.m_vecVelocity = Program.Offsets["DTBASEPLAYERMVECVELOCITY[0]"].Item2;

            OffsetsCSGO.NetVars.LocalPlayer.m_vecViewOffset = Program.Offsets["DTBASEPLAYERMVECVIEWOFFSET[0]"].Item2;
            OffsetsCSGO.NetVars.LocalPlayer.m_vecPunch = Program.Offsets["DTLOCALMVECPUNCH"].Item2;
            OffsetsCSGO.NetVars.LocalPlayer.m_iShotsFired = Program.Offsets["DTCSPLAYERMISHOTSFIRED"].Item2;
            OffsetsCSGO.NetVars.LocalPlayer.m_iCrosshairIdx = Program.Offsets["DTLOCALMICROSSHAIRID"].Item2;

            OffsetsCSGO.NetVars.Weapon.m_iItemDefinitionIndex = 0x131C;
            OffsetsCSGO.NetVars.Weapon.m_iState = Program.Offsets["DTBASECOMBATWEAPONMISTATE"].Item2;
            OffsetsCSGO.NetVars.Weapon.m_iClip1 = Program.Offsets["DTBASECOMBATWEAPONMICLIP1"].Item2;
            OffsetsCSGO.NetVars.Weapon.m_flNextPrimaryAttack = Program.Offsets["DTBASECOMBATWEAPONMFLNEXTPRIMARYATTACK"].Item2;
            OffsetsCSGO.NetVars.Weapon.m_iWeaponID = Program.Offsets["DTBASECOMBATWEAPONMIWEAPONID"].Item2;
            OffsetsCSGO.NetVars.Weapon.m_bCanReload = Program.Offsets["DTBASECOMBATWEAPONMBCANRELOAD"].Item2;
            OffsetsCSGO.NetVars.Weapon.m_iWeaponTableIndex = Program.Offsets["WEAPONTABLEMDWWEAPONTABLEINDEX"].Item2;
            OffsetsCSGO.NetVars.Weapon.m_fAccuracyPenalty = Program.Offsets["DTWEAPONCSBASEMFACCURACYPENALTY"].Item2;
            OffsetsCSGO.NetVars.Weapon.m_zoomLevel = Program.Offsets["DTWEAPONTASER_MZOOMLEVEL"].Item2;

            //TODO: etc.
        }

        private string InitClassOffsets()
        {
            StringBuilder offsetsLine = new StringBuilder();

                          offsetsLine.AppendLine("        public class Misc                                             ");
                          offsetsLine.AppendLine("        {                                                             ");
            offsetsLine.AppendLine(string.Format("            public static int EntityList = {0};                       ", Program.Offsets["ENTITYLISTMDWENTITYLIST"].Item1));
            offsetsLine.AppendLine(string.Format("            public static int LocalPlayer = {0};                      ", Program.Offsets["LOCALPLAYERMDWLOCALPLAYER"].Item1));
            offsetsLine.AppendLine(string.Format("            public static int Jump = {0};                             ", Program.Offsets["EXTRAMDWFORCEJUMP"].Item1));
            offsetsLine.AppendLine(string.Format("            public static int GlowManager = {0};                      ", Program.Offsets["EXTRAMDWGLOWOBJECT"].Item1));
            offsetsLine.AppendLine(string.Format("            public static int SignOnState = {0};                      ", Program.Offsets["CLIENTSTATEMDWINGAME"].Item1));
            offsetsLine.AppendLine(string.Format("            public static int WeaponTable = {0};                      ", Program.Offsets["WEAPONTABLEMDWWEAPONTABLE"].Item1));
            offsetsLine.AppendLine(string.Format("            public static int ViewMatrix = {0};                       ", Program.Offsets["ENGINERENDERMDWVIEWMATRIX"].Item1));
                          offsetsLine.AppendLine("        }                                                             ");
                          offsetsLine.AppendLine("                                                                      ");

                          offsetsLine.AppendLine("        public class ClientState                                      ");
                          offsetsLine.AppendLine("        {                                                             ");
            offsetsLine.AppendLine(string.Format("            public static int Base = {0};                             ", Program.Offsets["CLIENTSTATEMDWCLIENTSTATE"].Item1));
            offsetsLine.AppendLine(string.Format("            public static int m_dwInGame = {0};                       ", Program.Offsets["CLIENTSTATEMDWINGAME"].Item1));
            offsetsLine.AppendLine(string.Format("            public static int m_dwViewAngles = {0};                   ", Program.Offsets["CLIENTSTATEMDWVIEWANGLES"].Item1));
            offsetsLine.AppendLine(string.Format("            public static int m_dwMapname = {0};                      ", Program.Offsets["CLIENTSTATEMDWMAPNAME"].Item1));
            offsetsLine.AppendLine(string.Format("            public static int m_dwMapDirectory = {0};                 ", Program.Offsets["CLIENTSTATEMDWMAPDIRECTORY"].Item1));
                          offsetsLine.AppendLine("        }                                                             ");
                          offsetsLine.AppendLine("                                                                      ");

                          offsetsLine.AppendLine("        public class GameResources                                    ");
                          offsetsLine.AppendLine("        {                                                             ");
            offsetsLine.AppendLine(string.Format("            public static int Base = {0};                             ", 0x04A38E2C));
            offsetsLine.AppendLine(string.Format("            public static int Names = {0};                            ", 0x9D0));
            offsetsLine.AppendLine(string.Format("            public static int Kills = {0};                            ", 0xBD8));
            offsetsLine.AppendLine(string.Format("            public static int Assists = {0};                          ", 0xCDC));
            offsetsLine.AppendLine(string.Format("            public static int Deaths = {0};                           ", 0xDE0));
            offsetsLine.AppendLine(string.Format("            public static int Armor = {0};                            ", 0x182C));
            offsetsLine.AppendLine(string.Format("            public static int Score = {0};                            ", 0x192C));
            offsetsLine.AppendLine(string.Format("            public static int Clantag = {0};                          ", 0x4110));
                          offsetsLine.AppendLine("        }                                                             ");
                          offsetsLine.AppendLine("                                                                      ");

                          offsetsLine.AppendLine("        public class NetVars                                          ");
                          offsetsLine.AppendLine("        {                                                             ");

                          offsetsLine.AppendLine("              public class C_BaseEntity                               ");
                          offsetsLine.AppendLine("              {                                                       ");
            offsetsLine.AppendLine(string.Format("                  public static int m_iHealth = {0};                  ", Program.Offsets["DTBASEPLAYERMIHEALTH"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_iID = {0};                      ", Program.Offsets["BASEENTITYMDWINDEX"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_iTeamNum = {0};                 ", Program.Offsets["DTBASEENTITYMITEAMNUM"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_vecOrigin = {0};                ", Program.Offsets["DTBASEENTITYMVECORIGIN"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_angRotation = {0};              ", Program.Offsets["DTBASEENTITY_MANGROTATION"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_bSpotted = {0};                 ", Program.Offsets["DTBASEENTITYMBSPOTTED"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_bSpottedByMask = {0};           ", Program.Offsets["DTBASEENTITYMBSPOTTEDBYMASK"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_hOwnerEntity = {0};             ", Program.Offsets["DTBASEENTITYMHOWNERENTITY"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_bDormant = {0};                 ", Program.Offsets["BASEENTITYMBDORMANT"].Item1));
                          offsetsLine.AppendLine("              }                                                       ");
                          offsetsLine.AppendLine("                                                                      ");

                          offsetsLine.AppendLine("              public class C_CSPlayer                                 ");
                          offsetsLine.AppendLine("              {                                                       ");
            offsetsLine.AppendLine(string.Format("                  public static int m_lifeState = {0};                ", Program.Offsets["DTBASEPLAYERMLIFESTATE"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_iArmor = {0};                   ", Program.Offsets["DTCSPLAYERMARMORVALUE"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_hBoneMatrix = {0};              ", Program.Offsets["BASEENTITYMDWBONEMATRIX"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_hActiveWeapon = {0};            ", Program.Offsets["DTBASEPLAYERMHACTIVEWEAPON"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_iFlags = {0};                   ", Program.Offsets["DTBASEPLAYERMFFLAGS"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_hObserverTarget = {0};          ", Program.Offsets["DTBASEPLAYER_MHOBSERVERTARGET"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_iObserverMode = {0};            ", Program.Offsets["DTBASEPLAYER_MIOBSERVERMODE"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_vecVelocity = {0};              ", Program.Offsets["DTBASEPLAYERMVECVELOCITY[0]"].Item1));
                          offsetsLine.AppendLine("              }                                                       ");
                          offsetsLine.AppendLine("                                                                      ");

                          offsetsLine.AppendLine("              public class LocalPlayer                                ");
                          offsetsLine.AppendLine("              {                                                       ");
            offsetsLine.AppendLine(string.Format("                  public static int m_vecViewOffset = {0};            ", Program.Offsets["DTBASEPLAYERMVECVIEWOFFSET[0]"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_vecPunch = {0};                 ", Program.Offsets["DTLOCALMVECPUNCH"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_iShotsFired = {0};              ", Program.Offsets["DTCSPLAYERMISHOTSFIRED"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_iCrosshairIdx = {0};            ", Program.Offsets["DTLOCALMICROSSHAIRID"].Item1));
                          offsetsLine.AppendLine("              }                                                       ");
                          offsetsLine.AppendLine("                                                                      ");

                          offsetsLine.AppendLine("              public class Weapon                                     ");
                          offsetsLine.AppendLine("              {                                                       ");
            offsetsLine.AppendLine(string.Format("                  public static int m_iItemDefinitionIndex = {0};     ", 0x131C));
            offsetsLine.AppendLine(string.Format("                  public static int m_iState = {0};                   ", Program.Offsets["DTBASECOMBATWEAPONMISTATE"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_iClip1 = {0};                   ", Program.Offsets["DTBASECOMBATWEAPONMICLIP1"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_flNextPrimaryAttack = {0};      ", Program.Offsets["DTBASECOMBATWEAPONMFLNEXTPRIMARYATTACK"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_iWeaponID = {0};                ", Program.Offsets["DTBASECOMBATWEAPONMIWEAPONID"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_bCanReload = {0};               ", Program.Offsets["DTBASECOMBATWEAPONMBCANRELOAD"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_iWeaponTableIndex = {0};        ", Program.Offsets["WEAPONTABLEMDWWEAPONTABLEINDEX"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_fAccuracyPenalty = {0};         ", Program.Offsets["DTWEAPONCSBASEMFACCURACYPENALTY"].Item1));
            offsetsLine.AppendLine(string.Format("                  public static int m_zoomLevel = {0};                ", Program.Offsets["DTWEAPONTASER_MZOOMLEVEL"].Item1));
                          offsetsLine.AppendLine("              }                                                       ");
                          offsetsLine.AppendLine("                                                                      ");

                          offsetsLine.AppendLine("        }                                                             ");
                          offsetsLine.AppendLine("                                                                      ");

            string classOffsets = offsetsLine.ToString();
            return classOffsets;
        }
    }
}
