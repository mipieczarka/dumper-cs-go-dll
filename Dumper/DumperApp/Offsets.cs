﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DumperApp
{
    public class Offsets
    {
        [DllImport(@"C:\Users\PC\Source\Repos\Dumper\Dumper\Debug\DumperLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Result(StringBuilder str);

        public Offsets()
        {
            
        }

        //-------------------------------------------------------
        public Dictionary<string, Tuple<string, int>> GetListOffsets()
        //-------------------------------------------------------
        {
            Dictionary<string, Tuple<string, int>> returnOffsets = new Dictionary<string, Tuple<string, int>>();

            string groupOffsets = null;
            foreach (var item in StringToListString(GetOffsets()))
            {
                string sLineOffset = item.Replace(" ", "").Replace("|", "").Replace("_", "").Replace("-", "").Replace(">", "").Replace("<", "").Replace(":", "").Replace(";", "");

                if (sLineOffset.Contains("0x"))
                {
                    int iIndex = sLineOffset.LastIndexOf("0x");
                    string nameOffsets = string.Format("{0}_{1}", groupOffsets, sLineOffset.Substring(0, iIndex)).Trim().TrimStart('_').ToUpper();
                    string valueHexOffsets = sLineOffset.Substring(iIndex);
                    int valueIntOffsets = Convert.ToInt32(valueHexOffsets, 16);

                    if (returnOffsets.ContainsKey(nameOffsets))
                    {
                        if (returnOffsets[nameOffsets].Item1 != valueHexOffsets || returnOffsets[nameOffsets].Item2 != valueIntOffsets)
                        {
                            int numberOffsets = 1;
                            do
                            {
                                nameOffsets = string.Format("{0}_{1}", nameOffsets, ++numberOffsets).Trim().TrimStart('_').ToUpper();
                            }
                            while (returnOffsets.ContainsKey(nameOffsets));

                            if (!returnOffsets.ContainsKey(nameOffsets))
                            {
                                returnOffsets.Add(nameOffsets, new Tuple<string, int>(valueHexOffsets, valueIntOffsets));
                            }
                        }
                    }
                    else
                    {
                        returnOffsets.Add(nameOffsets, new Tuple<string, int>(valueHexOffsets, valueIntOffsets));
                    }
                }
                else
                {
                    groupOffsets = sLineOffset;
                }
            }

            return returnOffsets;
        }

        //-------------------------------------------------------
        private string GetOffsets()
        //-------------------------------------------------------
        {
            StringBuilder resultOffsets = null;
            string offsets = null;

            try
            {
                resultOffsets = new StringBuilder(100000000);
            }
            catch (Exception ex)
            {
                resultOffsets = null;
            }
            finally
            {
                if (resultOffsets != null)
                {
                    Result(resultOffsets);
                    offsets = resultOffsets.ToString();

                    resultOffsets.Clear();
                    resultOffsets = null;
                }
            }

            return offsets;
        }

        //-------------------------------------------------------
        private static List<string> StringToListString(string list, string sep = "\n")
        //-------------------------------------------------------
        {
            if (list == null)
                return new List<string>();
            else
                return list.Trim().Split(new[] { sep }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}
