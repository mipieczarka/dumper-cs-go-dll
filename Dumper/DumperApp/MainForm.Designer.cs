﻿namespace DumperApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_tbOffsets = new System.Windows.Forms.RichTextBox();
            this.m_gbOffsets = new System.Windows.Forms.GroupBox();
            this.m_gbOffsets.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_tbOffsets
            // 
            this.m_tbOffsets.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_tbOffsets.Location = new System.Drawing.Point(6, 19);
            this.m_tbOffsets.Name = "m_tbOffsets";
            this.m_tbOffsets.Size = new System.Drawing.Size(745, 408);
            this.m_tbOffsets.TabIndex = 0;
            this.m_tbOffsets.Text = "";
            // 
            // m_gbOffsets
            // 
            this.m_gbOffsets.Controls.Add(this.m_tbOffsets);
            this.m_gbOffsets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_gbOffsets.Location = new System.Drawing.Point(0, 0);
            this.m_gbOffsets.Name = "m_gbOffsets";
            this.m_gbOffsets.Size = new System.Drawing.Size(757, 433);
            this.m_gbOffsets.TabIndex = 1;
            this.m_gbOffsets.TabStop = false;
            this.m_gbOffsets.Text = "CS:GO";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 433);
            this.Controls.Add(this.m_gbOffsets);
            this.Name = "MainForm";
            this.Text = "Dumper";
            this.m_gbOffsets.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox m_tbOffsets;
        private System.Windows.Forms.GroupBox m_gbOffsets;
    }
}

